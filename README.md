What does it do?

It checks the logon locations for your office 365 tenants by going into the audit logs, finding the ip's (and user agents)
It then checks the location of the ip's using api.com (free service, u can pay to have the script run faster without api rate limit)
The default setting if country is not belgium or netherlands send an email to an alert adres.

Will it take me a minute to setup?
Nope, this will take some time

Doesn't microsoft have default security enabled for office 365 that warns me if they login from a strange country?
You would think so but no, there is a way to block logon locations but then u need to have a azure p2 contract per client.

I have 2FA for all my clients do I need this?
Yes, If they fish with reverse proxys like evilngnix2 https://github.com/kgretzky/evilginx2 

So after this I can sleep like a baby?
Nope, a good hacker will fish your client and use their device as a proxy (checkout beef https://beefproject.com/)
BUT at least u can check for badly coded phising campaigns (like most of them) that logon from strange user agents or strange locations.


SETUP:

EVERYONE WHO MANAGES OFFICE 365 NEEDS TO RUN STEP 1, EVEN IF U DO NOT WANT TO USE THE SCRIPT

1. Enable global audit log for the tenants (ITS TURNED OF BY DEFAULT????)
Turn on audit log search
In the Security & Compliance Center, go to Search > Audit log search. A banner is displayed saying that auditing has to be turned on to record user and admin activity.
Click Turn on auditing.

This will start logging all activity for the tenant.

Extra Info
at the time of writing access to the tenant comliance center using your partner account does not work, U will land on your company account
U have to go incognito and login as an admin from the tenant

While u are there u can also enable block account after x failed logins in x time.
Important: when using the default setting (block for 24 hours) even U as master admin cannot give the user access if blocked in 24h.
If u just select "block user" instead of "block user for 24h" u have the right to give him access after this happends

2. Wait at least 24hours until the logs are filled

3. Install microsoft powershell connector 
https://docs.microsoft.com/en-us/powershell/exchange/exchange-online/exchange-online-powershell-v2/exchange-online-powershell-v2?view=exchange-ps

U need this to be able to use 2FA
Do not spend 4 hours getting this to work on a mac, just use a vm or a windows computer

4. setup an email account for the alerts and a connector so u can send using port 25 without auth
https://docs.microsoft.com/en-us/exchange/mail-flow-best-practices/use-connectors-to-configure-mail-flow/set-up-connectors-to-route-mail

5. Edit lines in the script:
   8: partneremailaccount@yourcompany.com
   10: (-3) means last 3 days, could change upto 30
   38: country 1 to exclude from alerts
   39: country 2 to exclude from alerts
   55: alert from email adres (alerts@yourcompany.com)
   55: alert email address (alerts@yourcompany.com)
   55: smtp server, make a connector without auth (mx record from connector)

6. Run the powershell connector

7. Paste the first line of the script and logon with partner accounts

Connect-MsolService -Credential $credential

8. paste the rest of the scripted and wait for alerts to come in



Credits:
https://gcits.com/knowledge-base/export-list-locations-office-365-users-logging/

I basicly took their script, added the new exchange connector to be able to login with partner account, added location check and email alerts
